## INTRODUCTION

Simple xAPI lets you connect to an external xAPI-compliant system, and store [xAPI statements](https://en.wikipedia.org/wiki/Experience_API).

If you're building an eLearning platform or connecting 
Drupal to an external Learning Management System (LMS), 
Learning Record Store (LRS), or Learning Experience Platform (LXP), 
this module will interest you.

Simple xAPI buis based on the following projects:

* [D7 Tincan API](https://www.drupal.org/project/tincanapi), written 
by [nve](https://www.drupal.org/u/nve)
* Unofficial [D8 Tincan API port](https://github.com/devbisht/tincanapi) (github),
written by [devtherock](https://www.drupal.org/u/devtherock)

Reasons for starting a new d.o. project include:
* The "Tincan API" spec has been renamed to xAPI in 2016
* the D8 port needs a fair amount of cleanup and fixing of deprecated code
* the D8 port relies on jQuery, a dependency we'd like to eliminate
* both projects were stalled and/or abandoned

## Tracking Features

* Track node views (content type and view mode selection)

## Other Features
* Securely store API keys using config, separate file, or environment variable
* Real-time validation of endpoint credentials


## REQUIREMENTS

* [Key](https://www.drupal.org/project/key) module (for secure key storage)

## INSTALLATION

Install Simple xAPI using a standard method for installing a contributed Drupal
module.

## CONFIGURATION

Simple xAPI provides an administration page where users with the 
"administer simple_xapi" permission can manage endpoint connection 
details and other settings provided by submodules.

## Quickstart

### Step 1
* go to **admin/config/system/keys**
* create a key of the type _Authentication_, fill in your LRS account's 
username / shared key
* create another key of the type _Authentication_, fill in your LRS account's 
password / secret key

### Step 2
* go to **admin/config/services/xapi**  
* Provide your LRS' endpoint
* Select the keys you just created

Upon saving the settings, Drupal will test the connection credentials and
let you know if a connection to the endpoint could be established.

### Step 2
Enable one or more of Simple xAPI's submodules to make the module 
track actions / events and store the corresponding activity statements 
in the remote LRS.


## UPCOMING FEATURES
* Quiz support (track quiz submissions)
* Webform support (track webform submissions)
* Links support (track link clicks)
* Video player support (play, pause, stop, skip) 
for YouTube and Vimeo videos via the Media module
* Support for offline events
* Store statements and batch-retry later if the remote LRS is unresponsive 

## Get involved
Suggestions, bugs, and patches can be posted in the issue queue. 
If you would like your module to be included in the xAPI package 
please create a feature request.

Drupal.behaviors.simple_xapi_test_connection = {
  attach: function (context, settings) {

    // Attach a click event listener to the button.
    var btn = document.getElementById('edit-test');
    btn.addEventListener(
        'click', function () {
          var http = new XMLHttpRequest();
          var url = '/ajax/simplexapi/connection/check/statements/head';
          const READYSTATEDONE = 4;
          http.open('POST', url, true);

          http.onreadystatechange = function () {
            if(http.readyState == READYSTATEDONE) {

              // Create container element for response output.
              let response_container = document.createElement("div");
              response_container.classList.add('simple-xapi-ajax-response-container', 'messages');
              response_container.innerText = http.responseText;

              // Insert response container above the form action buttons.
              let form = document.getElementById("simple-xapi-admin-form");
              let form_actions = document.getElementById("edit-actions");
              form.insertBefore(response_container, form_actions);

            }
          }
          http.send();
        }, false
    );
  }
};

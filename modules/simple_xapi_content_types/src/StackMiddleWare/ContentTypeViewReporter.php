<?php

namespace Drupal\simple_xapi_content_types\StackMiddleware;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\simple_xapi\SimpleXAPI;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class Content Type View Reporter.
 *
 * @package Drupal\simple_xapi_content_types\StackMiddleware
 */
class ContentTypeViewReporter implements HttpKernelInterface {

  /**
   * The wrapped HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The SimpleXAPI Service.
   *
   * @var \Drupal\simple_xapi\SimpleXAPI
   */
  protected $xApi;

  /**
   * A logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a ContentTypeViewReporter object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\simple_xapi\SimpleXAPI $xApi
   *   The Simple xAPI service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   * @param \Drupal\Core\Session\AccountInterface $curentUser
   *   The current user.
   */
  public function __construct(HttpKernelInterface $http_kernel,
    SimpleXAPI $xApi,
    LoggerInterface $logger,
    RouteMatchInterface $routeMatch,
    AccountInterface $curentUser) {
    $this->httpKernel = $http_kernel;
    $this->xApi = $xApi;
    $this->logger = $logger;
    $this->currentRouteMatch = $routeMatch;
    $this->currentUser = $curentUser;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {

    // Do the next step in the chain first.
    $response = $this->httpKernel->handle($request, $type, $catch);

    try {

      $url = Url::fromRouteMatch($this->currentRouteMatch);
      if ($url->access($this->currentUser)) {
        if ($this->currentRouteMatch->getRouteName() === 'entity.node.canonical') {

          $node = $this->currentRouteMatch->getParameter('node');
          if ($this->isContentTypeToReport($node->getType())) {
            $this->reportNodeView($node);
          }
        }
      }
    }
    catch (\Error $e) {
      $this->logger->error('Caught error while trying to report to xAPI', ['exception' => $e]);
    }
    return $response;
  }

  /**
   * Check if given type is in the list to make the report.
   *
   * @param string $type
   *   Content type to check.
   *
   * @return bool
   *   Given type in the list or not.
   */
  protected function isContentTypeToReport($type) {
    $config = \Drupal::config('simple_xapi.settings');
    $content_types = array_filter($config->get('content_types_to_track'));
    return in_array($type, $content_types);
  }

  /**
   * Reports a node view to the LRS.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node that User viewed.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function reportNodeView(NodeInterface $node) {
    $statement = $this->xApi->createStatement('viewed');
    $statement->setObject(['id' => $node->toUrl('canonical', ['absolute' => TRUE])->toString()]);
    $this->xApi->send($statement);
  }

}

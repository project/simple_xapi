<?php

namespace Drupal\simple_xapi\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Statement annotation object.
 *
 * @ingroup statement
 *
 * @Annotation
 */
class XApiStatement extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the formatter type.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the formatter type.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The xAPI verb this statement represents.
   *
   * @var string
   */
  public $verb;

}

<?php

namespace Drupal\simple_xapi\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_xapi\SimpleXAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class SimpleXapiAdminForm extends ConfigFormBase {

  /**
   * SimpleXAPI service.
   *
   * @var \Drupal\simple_xapi\SimpleXAPI
   */
  protected $simpleXAPI;

  /**
   * Constructs a SimpleXapiAdminForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\simple_xapi\SimpleXAPI $simple_xapi
   *   SimpleXAPI service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, SimpleXAPI $simple_xapi) {
    parent::__construct($config_factory);
    $this->simpleXAPI = $simple_xapi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('simple_xapi')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_xapi_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['simple_xapi.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('simple_xapi.settings');

    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Connection details'),
    ];

    $form['connection']['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Provide the connection details for the remote system (usually a Learning Record System) where you want to store the xAPI statements.'),
    ];

    $form['connection']['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('The xAPI server endpoint. Example: https://example.com/trax/ws/xapi'),
      '#default_value' => $config->get('endpoint'),
      '#required' => TRUE,
    ];

    $form['connection']['auth_user_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Username or shared key'),
      '#default_value' => $config->get('auth_user_key'),
    ];

    $form['connection']['auth_password_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Password or secret key'),
      '#default_value' => $config->get('auth_password_key'),
    ];

    $form['logging'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Logging'),
    ];

    $form['logging']['error_logging_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log errors.'),
      '#default_value' => $config->get('error_logging_enabled'),
      '#description' => 'Log any errors encountered when communicating with your xAPI server.',
    ];

    $form['logging']['debug_logging_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log debug info.'),
      '#default_value' => $config->get('debug_logging_enabled'),
      '#description' => 'Log every request to your xAPI server.',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('simple_xapi.settings');

    // Trim a trailing slash from the endpoint if one was supplied.
    $config->set('endpoint', trim($form_state->getValue('endpoint'), '/'));
    $config->set('auth_user_key', $form_state->getValue('auth_user_key'));
    $config->set('auth_password_key', $form_state->getValue('auth_password_key'));
    $config->set('error_logging_enabled', $form_state->getValue('error_logging_enabled'));
    $config->set('debug_logging_enabled', $form_state->getValue('debug_logging_enabled'));
    $config->save();
    parent::submitForm($form, $form_state);

    // Check connection with supplied details.
    $this->simpleXAPI->testConnection();
  }

}

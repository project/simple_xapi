<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Completed' definition.
 *
 * @XApiStatement(
 *   id = "completed",
 *   label = @Translation("Completed"),
 *   description = @Translation("Records a user completed something."),
 *   verb = "http://adlnet.gov/expapi/verbs/completed"
 * )
 */
class Completed extends Statement {

}

<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Passed' definition.
 *
 * @XApiStatement(
 *   id = "failed",
 *   label = @Translation("Failed"),
 *   description = @Translation("Record an user when failed an activity."),
 *   verb = "http://adlnet.gov/expapi/verbs/failed"
 * )
 */
class Failed extends Statement {

  /**
   * Score Data.
   *
   * @var array
   */
  public $scoreData = [];

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $data = parent::getData();
    return array_merge($data, $this->scoreData);
  }

  /**
   * Set statement score.
   *
   * @param int $min
   *   Minimum score.
   * @param int $max
   *   Maximum score.
   * @param int $score
   *   Score.
   */
  public function setScore(int $min, int $max, int $score) {
    $this->scoreData = [
      'result' => [
        'completion' => TRUE,
        'score' => [
          'min' => $min,
          'max' => $max,
          'raw' => $score ?? $min,
          'scaled' => round($score / $max, 2) ?? $min,
        ],
        'success' => FALSE,
      ],
    ];
  }

}

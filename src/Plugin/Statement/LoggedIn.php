<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Logged In' definition.
 *
 * @XApiStatement(
 *   id = "logged-in",
 *   label = @Translation("Logged In"),
 *   description = @Translation("Records a user logging in."),
 *   verb = "https://w3id.org/xapi/adl/verbs/logged-in"
 * )
 */
class LoggedIn extends Statement {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $this->setObject(['id' => $this->host]);
    return parent::getData();
  }

}

<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Logged Out' definition.
 *
 * @XApiStatement(
 *   id = "logged-out",
 *   label = @Translation("Logged Out"),
 *   description = @Translation("Records a user logging out."),
 *   verb = "https://w3id.org/xapi/adl/verbs/logged-out"
 * )
 */
class LoggedOut extends Statement {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $this->setObject(['id' => $this->host]);
    return parent::getData();
  }

}

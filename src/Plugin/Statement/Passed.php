<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Passed' definition.
 *
 * @XApiStatement(
 *   id = "passed",
 *   label = @Translation("Passed"),
 *   description = @Translation("Record an user when passed an activity."),
 *   verb = "http://adlnet.gov/expapi/verbs/passed"
 * )
 */
class Passed extends Statement {

  /**
   * Score Data.
   *
   * @var array
   */
  public $scoreData = [];

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $data = parent::getData();
    return array_merge($data, $this->scoreData);
  }

  /**
   * Set statement score.
   *
   * @param int $min
   *   Minimum score.
   * @param int $max
   *   Maximum score.
   * @param int $score
   *   Score.
   */
  public function setScore(int $min, int $max, int $score) {
    $this->scoreData = [
      'result' => [
        'completion' => TRUE,
        'score' => [
          'min' => $min,
          'max' => $max,
          'raw' => $score ?? $max,
          'scaled' => round($score / $max, 2) ?? 1,
        ],
        'success' => TRUE,
      ],
    ];
  }

}

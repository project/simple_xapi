<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Registered' definition.
 *
 * @XApiStatement(
 *   id = "registered",
 *   label = @Translation("Registered"),
 *   description = @Translation("Record a user registering a learning activity."),
 *   verb = "http://adlnet.gov/expapi/verbs/registered"
 * )
 */
class Registered extends Statement {

}

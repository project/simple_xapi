<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Scored' definition.
 *
 * @XApiStatement(
 *   id = "scored",
 *   label = @Translation("Scored"),
 *   description = @Translation("Records a user submitted assessment and received a score."),
 *   verb = "http://adlnet.gov/expapi/verbs/scored"
 * )
 */
class Scored extends Statement {

}

<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Unregistered' definition.
 *
 * @XApiStatement(
 *   id = "unregistered",
 *   label = @Translation("Unregistered"),
 *   description = @Translation("Record a user unregistering a learning activity."),
 *   verb = "http://id.tincanapi.com/verb/unregistered"
 * )
 */
class Unregistered extends Statement {

}

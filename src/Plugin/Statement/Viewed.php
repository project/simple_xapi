<?php

namespace Drupal\simple_xapi\Plugin\Statement;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Plugin 'Viewed' definition.
 *
 * @XApiStatement(
 *   id = "viewed",
 *   label = @Translation("Viewed"),
 *   description = @Translation("Records a user viewing something."),
 *   verb = "http://id.tincanapi.com/verb/viewed"
 * )
 */
class Viewed extends Statement {

}

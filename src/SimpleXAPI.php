<?php

namespace Drupal\simple_xapi;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\simple_xapi\Statement\Statement;
use Psr\Log\LoggerInterface;

/**
 * The SimpleXAPI service.
 */
class SimpleXAPI implements SimpleXAPIInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Current User.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Authed Endpoint User.
   *
   * @var string
   */
  protected $endpointUser;

  /**
   * The Statement plugin manager.
   *
   * @var \Drupal\simple_xapi\StatementManager
   */
  protected $statementManager;

  /**
   * Authed User's password.
   *
   * @var string
   */
  protected $endpointPassword;

  /**
   * Request Header.
   *
   * @var array
   */
  protected $header = [
    'Content-Type' => 'application/json',
    'X-Experience-API-Version' => '1.0.3',
  ];

  /**
   * Endpoint Settings.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * Debug logging setting.
   *
   * @var bool
   */
  protected $debugLogging;

  /**
   * Error logging setting.
   *
   * @var bool
   */
  protected $errorLogging;

  /**
   * The Messenger to show up notice for test request.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a SimpleXAPI object.
   *
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The key.repository service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.factory service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The Current User.
   * @param \Drupal\simple_xapi\StatementManager $statementManager
   *   The Statement Manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Services.
   */
  public function __construct(
    KeyRepositoryInterface $key_repository,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    LoggerInterface $logger,
    AccountInterface $currentUser,
    StatementManager $statementManager,
    MessengerInterface $messenger
  ) {
    $config = $config_factory->get('simple_xapi.settings');
    $this->httpClient = $http_client;
    $this->logger = $logger;
    $this->currentUser = $currentUser;
    $this->statementManager = $statementManager;
    $this->messenger = $messenger;

    $this->endpoint = $config->get('endpoint');
    $this->debugLogging = $config->get('debug_logging_enabled');
    $this->errorLogging = $config->get('error_logging_enabled');
    if (!empty($config->get('auth_user_key'))) {
      $this->endpointUser = $key_repository
        ->getKey($config->get('auth_user_key'))
        ->getKeyValue();
    }
    else {
      $this->endpointUser = '';
    }
    if (!empty($config->get('auth_password_key'))) {
      $this->endpointPassword = $key_repository
        ->getKey($config->get('auth_password_key'))
        ->getKeyValue();
    }
    else {
      $this->endpointPassword = '';
    }
  }

  /**
   * Wraps the logger so we can control logging with config.
   *
   * @param string $level
   *   The level to log this message at.
   * @param string $message
   *   The message to log.
   */
  protected function log($level, $message) {

    if (($level === 'debug') && !$this->debugLogging) {
      return;
    }

    if (($level === 'error') && !$this->errorLogging) {
      return;
    }

    $this->logger->log($level, $message);
  }

  /**
   * Send Xapi Request with full data to end point.
   *
   * @param string $method
   *   XApi method: POST | GET | HEAD.
   * @param string $action
   *   Statement.
   * @param array $data
   *   Statement Data.
   *
   * @return array
   *   HTTP Response body and status.
   */
  protected function sendRequest($method, $action, array $data = []) {

    $param = [
      'auth' => [
        $this->endpointUser,
        $this->endpointPassword,
      ],
      'headers' => $this->header,
    ];

    $url = "{$this->endpoint}/{$action}";

    // If POST method, send $data as body $query parameter.
    // Otherwise use http_build_query to update url's query param.
    if ($method === 'POST') {
      $json = Json::encode($data);
      $param['headers']['Content-Length'] = strlen($json);
      $param['body'] = $json;
    }
    elseif ($method === 'GET') {
      // If any params are arrays, encode them as json:
      foreach ($data as $key => $value) {
        if (is_array($value)) {
          $data[$key] = Json::encode($value);
        }
      }
      $url .= '?' . http_build_query($data);
    }

    $this->log('info', 'Submitting ' . $method . ' request to "' . $url . '"');

    try {
      $response = $this->httpClient->request($method, $url, $param);
    }
    catch (GuzzleException $e) {
      $this->log('error', $e->getMessage());
      return [
        'status' => $e->getCode(),
        'body' => $e->getMessage(),
      ];
    }
    return [
      'status' => $response->getStatusCode(),
      'body' => $response->getBody(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function send(Statement $statement) {

    if ($statement->getActor()->hasPermission('skip xapi reporting')) {
      return NULL;
    }

    return $this->sendRequest('POST', 'statements', $statement->getData());
  }

  /**
   * {@inheritdoc}
   */
  public function createStatement($id) {
    return $this->statementManager->createInstance($id);
  }

  /**
   * Send Statement to endpoint.
   *
   * @deprecated in simple_xapi:8.x-1.1 and is removed from simple_xapi:8.x-1.2.
   *   Use ::send() instead.
   *
   * @see https://www.drupal.org/project/simple_xapi/issues/3166984
   */
  public function sendStatement($verb_id, $object_url, $optional_data = [], $actor = NULL) {

    // If the data hasn't set the actor, set currentUser as actor, provided they
    // don't have the permission to skip reporting.
    if (empty($actor)) {
      if ($this->currentUser->hasPermission('skip xapi reporting')) {
        return NULL;
      }
      $actor = $this->currentUser->getEmail();
    }

    $data['actor']['mbox'] = "mailto:" . $actor;
    $data['verb']['id'] = $verb_id;
    $data['object']['id'] = $object_url;

    // Set current time as $data's timestamp following ISO 8601 date format.
    if (empty($optional_data['timestamp'])) {
      $data['timestamp'] = date("c");
    }
    $data = array_merge($data, $optional_data);
    return $this->sendRequest('POST', 'statements', $data);
  }

  /**
   * Get Statement form Endpoint.
   *
   * @param array $query
   *   Full XApi Query param.
   *
   * @return array|mixed
   *   Endpoint Statement.
   */
  public function getStatements(array $query) {
    $response = $this->sendRequest('GET', 'statements', $query);
    $message = Json::decode($response['body']);
    return $message['statements'];
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection() {
    $response = $this->sendRequest('HEAD', 'statements');
    if ($response['status'] == 200) {
      $message = t('Endpoint connection check: success!');
      $this->messenger->addMessage($message, 'status');
      $this->logger->info($message);
    }
    else {
      $message = t('Endpoint connection check: failed with error:') . $response['body'];
      $this->messenger->addMessage($message, 'warning');
      $this->logger->warning($message);
    }
    return $response;
  }

}

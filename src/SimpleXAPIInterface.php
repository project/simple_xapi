<?php

namespace Drupal\simple_xapi;

use Drupal\simple_xapi\Statement\Statement;

/**
 * Interface SimpleXAPIInterface for Simple Xapi service.
 *
 * @package Drupal\simple_xapi
 */
interface SimpleXAPIInterface {

  /**
   * Accessed a uri.
   */
  const ACCESSED = "https://w3id.org/xapi/dod-isd/verbs/accessed";

  /**
   * Submitted answer to single question.
   */
  const ANSWERED = "http://adlnet.gov/expapi/verbs/answered";

  /**
   * Participated in (virtual) meeting/event, conference call, webinar, ...
   */
  const ATTENDED = "https://w3id.org/xapi/adb/verbs/attended";

  /**
   * Left a comment on a page/node/... or on another comment.
   */
  const COMMENTED = "http://adlnet.gov/expapi/verbs/commented";

  /**
   * Worked through a certain chapter or submitted all questions.
   */
  const COMPLETED = "http://adlnet.gov/expapi/verbs/completed";

  /**
   * Create a node or an entity.
   */
  const CREATED = "https://w3id.org/xapi/dod-isd/verbs/created";

  /**
   * Downloaded document, archive, ...
   */
  const DOWNLOADED = "http://id.tincanapi.com/verb/downloaded";

  /**
   * After completing assessment, user failed (or passed).
   */
  const FAILED = "http://adlnet.gov/expapi/verbs/failed";

  /**
   * After completing assessment, user passed (or failed).
   */
  const PASSED = "http://adlnet.gov/expapi/verbs/passed";

  /**
   * Received diploma / certificate / badge / ...
   */
  const RECEIVED = "https://w3id.org/xapi/dod-isd/verbs/received";

  /**
   * Submitted assessment and received a score.
   */
  const SCORED = "http://adlnet.gov/expapi/verbs/scored";

  /**
   * Started a course, filling in assessment, playing Video/audio, others.
   */
  const STARTED = "https://w3id.org/xapi/dod-isd/verbs/started";

  /**
   * Define 'accessed' over 'viewed'.
   */
  const VIEWED = "http://id.tincanapi.com/verb/viewed";

  /**
   * Indicates the actor gained access to a system or service.
   */
  const LOGGED_IN = "https://w3id.org/xapi/adl/verbs/logged-in";

  /**
   * Indicates the actor either lost or discontinued access to the system.
   */
  const LOGGED_OUT = "https://w3id.org/xapi/adl/verbs/logged-out";

  /**
   * Indicates the actor is officially enrolled or inducted in an activity.
   */
  const REGISTERED = "http://adlnet.gov/expapi/verbs/registered";

  /**
   * Indicates the actor unregistered for a learning activity.
   */
  const UNREGISTERED = "http://id.tincanapi.com/verb/unregistered";

  /**
   * Fetches statements.
   *
   * @param array $query
   *   Constraints on which statements to fetch.
   *
   * @return array
   *   Http response.
   */
  public function getStatements(array $query);

  /**
   * Create Statement Plugin.
   *
   * @param string $id
   *   Plugin ID.
   *
   * @return \Drupal\simple_xapi\Statement\Statement
   *   XAPI Statement fully configured plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createStatement($id);

  /**
   * Send a Statement to the LRS.
   *
   * @param \Drupal\simple_xapi\Statement\Statement $statement
   *   The statement to send.
   *
   * @return array
   *   Http response.
   */
  public function send(Statement $statement);

  /**
   * Test the connection to the LRS.
   *
   * @return array
   *   Http response.
   */
  public function testConnection();

}

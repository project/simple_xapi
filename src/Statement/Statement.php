<?php

namespace Drupal\simple_xapi\Statement;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract Class Statement.
 *
 * @package Drupal\simple_xapi\Statement
 */
abstract class Statement extends PluginBase implements StatementInterface {

  /**
   * The Actor.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $actor;

  /**
   * An array of verb data.
   *
   * @var array
   */
  protected $verb = [];

  /**
   * An array of object data.
   *
   * @var array
   */
  protected $object = [];

  /**
   * The current scheme and host. EG http://www.example.com.
   *
   * @var string
   */
  protected $host;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $currentUser, $host) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->actor = $currentUser;
    $this->verb['id'] = $plugin_definition['verb'];
    $this->host = $host;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setActor(AccountInterface $actor) {
    $this->actor = $actor;
  }

  /**
   * {@inheritdoc}
   */
  public function getActor():AccountInterface {
    return $this->actor;
  }

  /**
   * {@inheritdoc}
   */
  public function getVerb(): array {
    return $this->verb;
  }

  /**
   * {@inheritdoc}
   */
  public function setVerb(array $verb): void {
    $this->verb = $verb;
  }

  /**
   * {@inheritdoc}
   */
  public function getObject(): array {
    return $this->object;
  }

  /**
   * {@inheritdoc}
   */
  public function setObject(array $object): void {
    $this->object = $object;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {

    return [
      'actor' => [
        'name' => $this->actor->getDisplayName(),
        'account' => [
          'name' => $this->actor->id(),
          'homePage' => $this->host,
        ],
      ],
      'verb' => $this->getVerb(),
      'object' => $this->getObject(),
      'timestamp' => date('c'),
    ];
  }

}

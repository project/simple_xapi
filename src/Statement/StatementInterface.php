<?php

namespace Drupal\simple_xapi\Statement;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Interface StatementInterface for Statement Plugin.
 *
 * @package Drupal\simple_xapi\Statement
 */
interface StatementInterface extends ContainerFactoryPluginInterface {

  /**
   * Set the Actor of statement.
   *
   * @param \Drupal\Core\Session\AccountInterface $actor
   *   Current User or another one.
   */
  public function setActor(AccountInterface $actor);

  /**
   * Get the Actor.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   Current User or another one.
   */
  public function getActor();

  /**
   * Get Verb value.
   *
   * @return array
   *   Verb Data.
   */
  public function getVerb();

  /**
   * Set Verb value.
   *
   * @param array $verb
   *   Verb.
   */
  public function setVerb(array $verb);

  /**
   * Get Object in Statement Data.
   *
   * @return array
   *   The Object.
   */
  public function getObject();

  /**
   * Set statement's object.
   *
   * @param array $object
   *   The Object.
   */
  public function setObject(array $object);

  /**
   * Assembles the data to send over xAPI for this statement.
   *
   * @return array
   *   Full Statement Data.
   */
  public function getData();

}

<?php

namespace Drupal\Tests\simple_xapi\Unit;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Session\AccountInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\simple_xapi\SimpleXAPI;
use Drupal\simple_xapi\StatementManager;
use Drupal\Tests\simple_xapi\Unit\ConcreteStatement as Statement;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * Tests SimpleXAPITest.
 *
 * @group _simple_xapi
 */
class SimpleXAPITest extends UnitTestCase {

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $httpClient;

  /**
   * The Logger.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $logger;

  /**
   * The Key Repository.
   *
   * @var \Drupal\Tests\simple_xapi\Unit\KeyRepositoryStub
   */
  protected $keyRepository;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockBuilder
   */
  protected $configFactory;

  /**
   * The Current User.
   *
   * @var \Drupal\Core\Session\AccountInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $currentUser;

  /**
   * The Simple Xapi Statement Manager.
   *
   * @var \Drupal\simple_xapi\StatementManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $statementManager;

  /**
   * The Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->httpClient = $this->createMock(ClientInterface::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->keyRepository = new KeyRepositoryStub([
      'auth_user_key' => 'auth_user',
      'auth_password_key' => 'auth_password',
    ]);
    $this->configFactory = $this->getConfigFactoryStub([
      'simple_xapi.settings' => [
        'endpoint' => 'http://example.com/ws/xapi',
        'debug_logging_enabled' => FALSE,
        'error_logging_enabled' => FALSE,
        'auth_user_key' => 'auth_user_key',
        'auth_password_key' => 'auth_password_key',
      ],
    ]);

    $this->currentUser = $this->createMock(AccountInterface::class);
    $this->statementManager = $this->createMock(StatementManager::class);
    $this->messenger = $this->createMock(MessengerInterface::class);
  }

  /**
   * {@inheritdoc}
   */
  public function testGetRequest() {

    $xAPI = new SimpleXAPI($this->keyRepository,
      $this->configFactory,
      $this->httpClient,
      $this->logger,
      $this->currentUser,
      $this->statementManager,
      $this->messenger);

    $response = new Response(200, [], Json::encode(['statements' => 'Something here']));

    $this->httpClient
      ->expects($this->once())
      ->method('request')
      ->with('GET', 'http://example.com/ws/xapi/statements?agent=%7B%22mbox%22%3A%22mailto%3Aemail%40example.com%22%7D')
      ->willReturn($response);

    $data = [
      'agent' => [
        'mbox' => 'mailto:email@example.com',
      ],
    ];

    $this->assertSame('Something here', $xAPI->getStatements($data));
  }

  /**
   * {@inheritdoc}
   */
  public function testPostRequest() {

    $xAPI = new SimpleXAPI($this->keyRepository,
      $this->configFactory,
      $this->httpClient,
      $this->logger,
      $this->currentUser,
      $this->statementManager,
      $this->messenger);

    $response = new Response(200, [], '');

    $this->httpClient
      ->expects($this->once())
      ->method('request')
      ->with('POST', 'http://example.com/ws/xapi/statements', [
        'auth' => [
          'auth_user',
          'auth_password',
        ],
        'headers' => [
          'Content-Type' => 'application/json',
          'X-Experience-API-Version' => '1.0.3',
          'Content-Length' => 538,
        ],
        'body' => '{"actor":{"name":"Namey McNameface","account":{"name":123,"homePage":"http:\/\/example.com"}},"verb":{"id":"http:\/\/adlnet.gov\/expapi\/verbs\/attended","display":{"en-US":"attended"}},"object":{"id":"https:\/\/app.livestorm.co\/p\/980f5996-18fc-44e5-9d62-e9c6700ae7a6\/live?s=2d1fbe8f-ef3b-485c-9156-0e0074309013","definition":{"name":{"en-US":"Webinar"},"description":{"en-US":"Title-of-the-livestorm-webinar"},"type":"http:\/\/id.tincanapi.com\/activitytype\/webinar"},"objectType":"Activity"},"timestamp":"' . date('c') . '"}',
      ])
      ->willReturn($response);

    $plugin_definition = [
      'verb' => 'http://adlnet.gov/expapi/verbs/attended',
    ];

    $currentUser = $this->createMock(AccountInterface::class);
    $currentUser->method('getDisplayName')->willReturn('Namey McNameface');
    $currentUser->method('id')->willReturn(123);

    $statement = new Statement([], '', $plugin_definition, $currentUser, 'http://example.com');
    $statement->setVerb([
      'id' => 'http://adlnet.gov/expapi/verbs/attended',
      'display' => [
        'en-US' => 'attended',
      ],
    ]);
    $statement->setObject([
      "id" => "https://app.livestorm.co/p/980f5996-18fc-44e5-9d62-e9c6700ae7a6/live?s=2d1fbe8f-ef3b-485c-9156-0e0074309013",
      "definition" => [
        "name" => [
          "en-US" => "Webinar",
        ],
        "description" => [
          "en-US" => "Title-of-the-livestorm-webinar",
        ],
        "type" => "http://id.tincanapi.com/activitytype/webinar",
      ],
      "objectType" => "Activity",
    ]);

    $result = $xAPI->send($statement);

    $this->assertSame(200, $result['status']);
    $this->assertSame('', (string) $result['body']);
  }

}

/**
 * Class KeyRepositoryStub.
 *
 * @package Drupal\Tests\simple_xapi\Unit
 */
class KeyRepositoryStub implements KeyRepositoryInterface {

  /**
   * {@inheritdoc}
   */
  protected $values;

  /**
   * {@inheritdoc}
   */
  public function __construct($values) {
    $this->values = $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getKey($name) {
    return new KeyStub($this->values[$name]);
  }

  /**
   * {@inheritdoc}
   */
  public function getKeys(array $key_ids = NULL) {}

  /**
   * {@inheritdoc}
   */
  public function getKeysByProvider($key_provider_id) {}

  /**
   * {@inheritdoc}
   */
  public function getKeysByType($key_type_id) {}

  /**
   * {@inheritdoc}
   */
  public function getKeysByStorageMethod($storage_method) {}

  /**
   * {@inheritdoc}
   */
  public function getKeysByTypeGroup($type_group) {}

  /**
   * {@inheritdoc}
   */
  public function getKeyNamesAsOptions(array $filters) {}

}

/**
 * Class KeyStub.
 *
 * @package Drupal\Tests\simple_xapi\Unit
 */
class KeyStub {

  /**
   * {@inheritdoc}
   */
  protected $value;

  /**
   * {@inheritdoc}
   */
  public function __construct($value) {
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue() {
    return $this->value;
  }

}

<?php

namespace Drupal\Tests\simple_xapi\Unit;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\simple_xapi\Unit\ConcreteStatement as Statement;
use Drupal\Tests\UnitTestCase;

/**
 * Tests \Drupal\simple_xapi\Statement\Statement via ConcreteStatement.
 *
 * @group simple_xapi
 */
class StatementTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function testGetData() {

    $configuration = [];
    $plugin_id = '';
    $plugin_definition = [
      'verb' => 'http://example.com/verb',
    ];

    $currentUser = $this->createMock(AccountInterface::class);
    $currentUser->method('getDisplayName')->willReturn('Namey McNameface');
    $currentUser->method('id')->willReturn(123);

    $statement = new Statement($configuration, $plugin_id, $plugin_definition, $currentUser, 'http://example.com');
    $statement->setObject(['id' => 'http://example.com/object']);

    $expected = [
      'actor' => [
        'name' => 'Namey McNameface',
        'account' => [
          'name' => 123,
          'homePage' => 'http://example.com',
        ],
      ],
      'verb' => [
        'id' => 'http://example.com/verb',
      ],
      'object' => [
        'id' => 'http://example.com/object',
      ],
      // This is slightly dodgy but seems to work.
      'timestamp' => date('c'),
    ];

    $this->assertEquals($expected, $statement->getData());
  }

}
